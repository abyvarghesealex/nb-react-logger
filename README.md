# Welcome to nb-react-logger 👋
[![Version](https://img.shields.io/npm/v/nb-react-logger.svg)](https://www.npmjs.com/package/nb-react-logger)
[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](#)

> logging system for api performance and console, javascript errors. a proper axios instance and a config object with POST_URL (where you want to post the logs to) has to be provided at initialisation to the initializeAll function

## Install

```sh
npm i nb-react-logger
```

## Usage

```sh
import { initializeAll } from "nb-react-logger";
initalizeAll(axios, {POST_URL:"your url"})
```

## Author

👤 **aby varghese alex**
