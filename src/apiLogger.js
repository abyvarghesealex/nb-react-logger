import {API_THRESHHOLD} from './constants';

const checkTimeAndTrigger = (response) => {
  if (response.duration > API_THRESHHOLD) {
    console.info('API_THRESHHOLD', response);
  }
};
const ApiLogger = (axios) => {
  axios.interceptors.request.use(
    (config) => {
      config.metadata = { startTime: new Date() };
      console.log('request success working');
      return config;
    },
    (error) => {
      console.log('request error working');
      return Promise.reject(error);
    }
  );
  axios.interceptors.response.use(
    (response) => {
      console.log('response success working');
      response.config.metadata.endTime = new Date();
      response.duration = response.config.metadata.endTime - response.config.metadata.startTime;
      checkTimeAndTrigger(response);
      return response;
    },
    (error) => {
      console.log('response error working');
      error.config.metadata.endTime = new Date();
      error.duration = error.config.metadata.endTime - error.config.metadata.startTime;
      checkTimeAndTrigger(error);
      return Promise.reject(error);
    }
  );
};

export default ApiLogger;
