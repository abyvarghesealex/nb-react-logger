import axios from 'axios';
import {POST_URL, HEADERS} from './constants';

const errorsNotSendLog = (message) => [
  'Failed prop type:',
  'Warning: React does not recognize',
  'Warning: Received',
  'Warning: Expected server HTML',
  'Warning: Each child in a list should have a unique "key"',
  'Encountered two children with the same key',
  'A component is changing a controlled input',
  'prop on `%s` should not be null'
].some((item) => message && message.includes && message.includes(item));
const triggerLog = (...args) => {
  const data = {};
  let triggerCall = true;
  args.forEach((item, index) => {
    if (item instanceof Error) {
      data.error = item.message;
      data.stack = item.stack ? JSON.stringify(item.stack) : '';
    } else if (typeof item === 'object') {
      data[index] = JSON.stringify(item);
    } else {
      data[index] = item;
    }
    if (errorsNotSendLog(item)) {
      triggerCall = false;
    }
  });
  if (triggerCall) {
    const reqObj = {
      timeStamp: new Date().toString(),
      data,
      version: navigator.userAgent
    };
    if ('requestIdleCallback' in window) {
      requestIdleCallback(() => {
        axios.post(POST_URL, reqObj, HEADERS);
      });
    } else {
      axios.post(POST_URL, reqObj, HEADERS);
    }
  }
};

const LoggerifyConsole = () => {
  // && process.env.NODE_ENV === 'production'
  if (window && window.console) {
    const originalConsole = { ...window.console };
    window.console = {
      ...window.console,
      info: (...args) => {
        originalConsole.info(...args);
        triggerLog(...args);
      },
      error: (...args) => {
        originalConsole.error(...args);
        triggerLog(...args);
      }
      // warn: (...args) => {
      //   originalConsole.warn(...args);
      //   triggerLog(...args);
      // }
    };
  }
};

export default LoggerifyConsole;
