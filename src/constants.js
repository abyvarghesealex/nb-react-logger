
let API_THRESHHOLD = 5000;
let POST_URL = '';
let HEADERS = {
  headers: {
    Accept: 'application/json'
  },
  withCredentials: true
};
const setVals = (vals = {}) => {
  API_THRESHHOLD = vals.API_THRESHHOLD || API_THRESHHOLD;
  POST_URL = vals.POST_URL || POST_URL;
  HEADERS = vals.HEADERS || HEADERS;
}


export {
  API_THRESHHOLD,
  POST_URL,
  HEADERS,
  setVals
};
