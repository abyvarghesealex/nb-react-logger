import invariant from "invariant";
import LoggerifyConsole from "./clientLogger";
import ApiLogger from "./apiLogger";
import { setVals } from "./constants";

const checkAllValidConditions = (axios, config) => {
  invariant(typeof axios === "function", "the first parameter should be a valid axios object");
  invariant(typeof config === "object" && config.POST_URL,'the second parameter  should be valid config object with atleast POST_URL defined')
};

const initializeAll = (axios, config) => {
  checkAllValidConditions(axios, config);
  setVals(config);
  LoggerifyConsole();
  ApiLogger(axios);
};

export { LoggerifyConsole, ApiLogger, initializeAll };
